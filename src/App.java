import java.util.Arrays; 
import java.util.Scanner;
public class App {
 // affiche les points 
 public static void display (int [] input)
 {  System.out.println("affichage du tableau des points");
     for (int i=0;i!=input.length;i++)
     {
         System.out.println(input[i]);
     }
 }
 public static void displayocc (int [] input,int numero)
 {  System.out.println("affichage du tableau d'occurences pour le joueur: "+numero);
     for (int i=0;i!=input.length;i++)
     {
         System.out.println(input[i]);
     }
 }
 public static void display22 (int [] input,int numero)
 {  System.out.println("affichage du jet de dés du joueur:"+numero);
     for (int i=0;i!=input.length;i++)
     {
         System.out.println(input[i]);
     }
 }
 // méthode renvoyant un tableau des cases non encore remplies

 public static void displaycheck (boolean [] check,boolean [] condi)
 {  System.out.println("affichage des choix possibles ");
     for (int i=0;i!=6;i++)
     {   
         if (check[i]==false)
         {System.out.println("vous pouvez choisir la case: "+i);
          System.out.println("qui correspond à la valeur de dé: "+(i+1));}
     }
     for (int i=6;i!=12;i++)
     {
         if ((check[i]==false)&&(condi[i-6]))
         {
            System.out.println("vous pouvez choisir la case: "+i);  
         }
     }
     if (check[12]==false)
     {System.out.println("vous pouvez aussi cocher la case chance: entrer la valeur 12 ");}
 }
 public static int [] lancer (int [] init)
 {
    int [] randy= new int [init.length];
    for (int k=0;k!=5;k++)
    {randy[k]=(int)(Math.random()*6+ 1);
        
    }
    return randy;
 }
 // méthode qui gère les trois lancers à insérer
 public static int [] lancer3dés (int [] init)
 {  int [] jeumod=new int [5];
     boolean fdl=false;
     int [] jeu=lancern(init, 5);
         display22(jeu,1);
         Scanner scanner = new Scanner(System.in);
         int max=0; // compteur du nombre de lancers
    
     System.out.println("Voulez vous rejouer vos dés?");
     int xx= scanner.nextInt();
     if (xx!=1)
     {
         fdl=true; // fin des lancers
         jeumod=Arrays.copyOf(jeu, 5); // sauvegarde du jet 
     }
     else 
     {while ((fdl==false)&&(max!=2))
     {
         
         
         
        int numerodudé=0;
           while(numerodudé!=5)
        {
        System.out.println("Taper 1 pour garder le dé numéro: "+(numerodudé+1));
        int choix = scanner.nextInt();
        if (choix==1)
        {jeumod[numerodudé]=jeu[numerodudé];}             // garde la valeur du dé 
        else {
            jeumod[numerodudé]=(int)(Math.random()*6+ 1); // rejoue le dé
        }
            numerodudé=numerodudé+1;
        }
    
    System.out.println("Vos dés après nouveau tirage: ");   
    display22(jeumod,1);    
    max=max+1;
    if (max!=2)
    {System.out.println("Voulez vous rejouer vos dés?");
    int xxx=scanner.nextInt();
    if (xxx!=1)
    {fdl=true;
    }}
     }
    }
     
     return jeumod;
 }

 // méthode qui renvoie un jet de nbddés non
 public static int [] lancern (int [] init,int nbdedés)
 {
    int [] randy= new int [nbdedés];
    for (int k=0;k!=nbdedés;k++)
    {randy[k]=(int)(Math.random()*6+ 1);
        
    }
    return randy;
 }

// clacule les occurences
 public static int [] occurences (int [] results)
 {   int [] score=new int [results.length+1];
    for (int j=0;j!=6;j++)
        {
        for (int i=0;i!=5;i++)
    {
        if (results[i]==j+1)
        score[j]=score[j]+1;
    }}
    

    return score;



 }
// méthode renvoyant la valeur du dé d'un brelan (3 en argument) ou d'un carré (4)
public static int get(int [] occur, int nnn)

{   int value=0;
    for (int k=0;k!=6;k++)
    {if (occur[k]==nnn)
    value=k+1;}
    return value;
}

 public static boolean brelan (int [] occur,boolean [] checkytab1)
 {  boolean inter=false;
    boolean brelan1=false;
    for (int i=0;i!=6;i++)
    {if (occur[i]==3)

        {
        System.out.println("le joueur 1 a un brelan pour la valeur: "+(i+1));
        // attention mauvais score!!!
        System.out.println("Cela correspond à un score de: "+(i+1)*3);
        brelan1=true;
        }
    }
    if ((brelan1==true)&&(checkytab1[6]==false))
    {   
        System.out.println("vous pouvez choisir ce brelan");
        inter=true;}
    return inter;
     
 }
 // détection d'une grande suite
 public static boolean grandesuite (int [] occur,boolean [] checkytab1)
 {
   
int[] grandesuite1 = new int[] {0,1, 1, 1, 1,1};
int[] grandesuite2 = new int[] {1,1, 1, 1, 1,0};
boolean grand=false;

if (Arrays.equals(grandesuite1,occur)||Arrays.equals(grandesuite2,occur))
{System.out.println("Grande suite pour le joueur !!!!");
System.out.println("Cela correspond à un score de 50 points");
if (checkytab1[10]==false) 
{grand=true;}
}

    return grand;

    

 }
 // méthode de détection d'un carré renvoit vrai si carré et carré non déjà rempli
 public static boolean carre (int[] occur,boolean [] checkytab1)
 {
    int valeurcarré=0;
    boolean square1=false;
    for (int i=0;i!=6;i++)
    {if (occur[i]==4)

        {valeurcarré=(i+1);
        System.out.println("le joueur 1 a un carré pour la valeur: "+(i+1));
        // attention calcul de score non valide!!!
        System.out.println("Cela correspond à un score de: "+(i+1)*4);
        
        if (checkytab1[7]==false) // vérifie si déjà placé
        {square1=true;
            System.out.println("Vous pouvez placer ce carré");

        }


        }
        
    }
    return square1;
 }

 // méthode pour la détection de yams
 public static boolean yams (int[] occur,boolean [] checkytab1)
    {
        int c0=0;
        int c5=0;
        boolean yami1=false;
        for (int i=0;i!=6;i++) // comptage des zéros et des 5
        {if (occur[i]==0)
        {
            c0=c0+1;
    
        }
        if (occur[i]==5)
        {c5=c5+1;}
    
        }   
        // test de yams ( cinq zéros et un 5 dans le tableau d'occurences)
        if ((c0==5)&&(c5==1))
        {System.out.println("Vous avez un Yams!!!!");
        if (checkytab1[11]==false)
        {System.out.println("Vous pouvez placer ce Yams!!!!");
        yami1=true;} 

        
    }
    return yami1;
    }
    public static boolean fully (int [] occur,boolean [] checkytab1)
    {
        // test de full, un brelan et une paire
    
    boolean b31=false;
    boolean b21=false;
    boolean fully1=false;
    for (int i=0;i!=6;i++)
    {
        if (occur[i]==3)
        {b31=true;
        }

    }
    for (int i=0;i!=6;i++)
    {if (occur[i]==2)
    {b21=true;

    }}
    if ((b21==true)&&(b31==true))
    {
        
        
        if (checkytab1[8]==false)
        {   System.out.println("Full!!!!");
            System.out.println("vous pouvez placer ce full!!!");
            fully1=true;}
        
    }
    return fully1;
    }
    public static void sc16 (int [] occur,boolean [] checkytab1)
    {
    for (int i=0;i!=6;i++)
    {   
        
        if (checkytab1[i]==false)
        {   System.out.println("score possible pour la valeur: "+(i+1));
            System.out.println(occur[i]*(i+1));
            }

    }
    

    }
    //méthode renvoyant true si petite suite 
    public static boolean petite (int [] occur, boolean [] checkytab1)
    {   boolean naine=false;
        // énumération des 14 petites suites
        int [] p1 ={1,1,1,1,0,1};
        int [] p2 ={2,1,1,1,0,0};
        int [] p3 ={1,2,1,1,0,0};
        int [] p4 ={1,1,2,1,0,0};
        int [] p5 ={1,1,1,2,0,0};
        int [] p6 ={0,2,1,1,1,0};
        int [] p7 ={0,1,2,1,1,0};
        int [] p8 ={0,1,1,2,1,0};
        int [] p9 ={0,1,1,1,2,0};
        int [] p10={1,0,1,1,1,1};
        int [] p11={0,0,2,1,1,1};
        int [] p12={0,0,1,2,1,1};
        int [] p13={0,0,1,1,2,1};
        int [] p14={0,0,1,1,1,2};
        boolean intx1=(Arrays.equals(p1,occur))||(Arrays.equals(p2,occur))||(Arrays.equals(p3,occur));
        boolean intx2=(Arrays.equals(p4,occur))||(Arrays.equals(p5,occur))||(Arrays.equals(p6,occur));
        boolean intx3=(Arrays.equals(p7,occur))||(Arrays.equals(p8,occur))||(Arrays.equals(p9,occur));
        boolean intx4=(Arrays.equals(p10,occur))||(Arrays.equals(p11,occur))||(Arrays.equals(p12,occur));
        boolean intx5=(Arrays.equals(p13,occur))||(Arrays.equals(p14,occur));
        if ((intx1==true)||(intx2==true)||(intx3==true)||(intx4==true)||(intx5==true))
        {   if (checkytab1[9]==false)
            {naine=true;
        System.out.println("vous avez une petite suite et vous pouvez la placer");}

        
            }
            return naine;
        

    } 
    /*    // méthode qui renvoie les choix possibles en fonction des occurences et des cases déjà remplies
        // non finie!!! et non utilisée dans le main
    public static String choixutilisateurs (boolean [] checkytab1,int [] occur)
    { // déclaration scanner
            // scanner à déclarer
            for(int k=0;k!=6;k++)
            {
                if (checkytab1[k]==false)
                {
                    System.out.println("choix possible:  "+(k+1));
                }
            } 
            if (brelan(occur, checkytab1)==true)
                    {System.out.println("entrer b pour brelan");}

           // entrée clavier   
           return "réponse";   
    }*/
    // méthode renvoyant le total des dés pour la case chance
    public static int chance (int [] results,boolean [] checkytab1)
    { int total5=0;
        if (checkytab1[12]==false)
        {
        for (int k=0;k!=5;k++)
        total5=total5+results[k];}
        return total5;
    }
    public static int scorefinal (int [] results)
    {
        int cpt=0;
        for (int k=0;k!=results.length;k++)
        {cpt=cpt+results[k];
        }
        return cpt;
    }

    public static void main(String[] args) throws Exception {
        
        // un tour de dés

        int [] initial={0,0,0,0,0};
        boolean [] checkytab1=new boolean[13]; // false si case pas encore cochée
        boolean [] endofgame=new boolean[13]; // endofgame est un tableau de 13 true
        int [] points = new int [13]; // tableau des points initialisés à 0
        // construction du tableau de 13 true
        for (int k=0;k!=13;k++)
        {endofgame[k]=true;}
        
        boolean manchefinie=false;
        // boucle tant que manchefinie égale false
        while (manchefinie==false)
        {int [] test=lancer3dés(initial); // jet de dés stocké dans test
        //display22(test,1); // affichage du jet de dés du joueur numero
        //1displayocc(occurences(test),1); // affichage des occurences de 1 à 6
        
        
        
       // tests des méthodes tableaux particulier
        /*int [] fullboy={0,5,0,0,0,0};
        int [] carreboy={4,1,0,0,0,0};
        int [] sweetie={0,2,1,1,1,0};
        boolean yu;
        boolean car;
        boolean yoman;
        yoman=petite(sweetie, checkytab1);
        yu=yams(fullboy,checkytab1);
        car=carre(carreboy,checkytab1);
        System.out.println("test lancer de taille 4");
        int [] test2=lancern(initial,4);
        
        // tests de détection d'un brelan

        // test pour fin de manche*/
        
        sc16(occurences(test), checkytab1);
        boolean x7=brelan(occurences(test),checkytab1);
        boolean x8=carre(occurences(test), checkytab1);
        boolean x9=fully(occurences(test), checkytab1);
        boolean x10=petite(occurences(test), checkytab1);
        boolean x11=grandesuite(occurences(test), checkytab1);
        boolean x12=yams(occurences(test), checkytab1);
        System.out.println("score chance: ");
        System.out.println(chance(test, checkytab1));
        boolean [] conditions={x7,x8,x9,x10,x11,x12};
        displaycheck(checkytab1,conditions);
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez votre choix ou 45 pour sortir du jeu");
        int choix = scanner.nextInt();
        //checkytab1[choix]=true; // enregistre le choix du joueur
        // calcul des scores et remplissage de la table des scores
        // à tester!!!
        
        /*if ((choix<0)||(choix>12))
        {System.out.println("vous de devez taper un chiffre compris entre 0 et 12");
        System.out.println("Entrez votre choix");
        int choix2= scanner.nextInt();
        }*/
        
        if (choix<=5)
        {points[choix]=(choix+1)*(occurences(test)[choix]);
        checkytab1[choix]=true;}
        // brelan
        if (choix==6)
        {points[choix]=3*get(occurences(test),3);
            checkytab1[choix]=true;}
        if (choix==7)
        {points[choix]=4*get(occurences(test),4);
            checkytab1[choix]=true;}
        if (choix==8)
        { checkytab1[8]=true;
        points[8]=25;}
        if (choix==9)
        {checkytab1[9]=true;
        points[9]=30;}
        if (choix==10)
        {checkytab1[10]=true;
        points[10]=40;}
        if (choix==11)
        {checkytab1[11]=true;
        points[11]=50;}
        if (choix==12)
        {points[12]=chance(test, checkytab1);
        checkytab1[12]=true;}
        display(points);
        // évaluation du remplissage du tableau de score
        // comparaison entre endofgame et checkytab1
        manchefinie=Arrays.equals(checkytab1,endofgame);
        if (choix==45)
        {
            manchefinie=true;
        }
        //manchefinie prend la valeur true si tableau entièrement rempli
        }
        
        display(points); 
        // calcul du total
        
        
                   
    }
    }